# Webcounter

Simple Python Webcounter with redis server

## Build
docker build -t cfreire70/webcounter:1.0.1 .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis cfreire70/webcounter:1.0.1

#gitlab Register

gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "docker-lab" \
--tag-list "production" \
--registration-token GR1348941yK-vZCqve1yMcnF37oHG

